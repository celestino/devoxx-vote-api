package cfp

import (
	"log"
	"net/url"
	"strings"

	"menteslibres.net/gosexy/rest"
)

type Link struct {
	Href  string `json:"href"`
	Title string `json:"title"`
	Rel   string `json:"rel"`
}

type Speaker struct {
	Name string `json:"name"`
	Link Link   `json:"link"`
}

type Speakers []Speaker

type Talk struct {
	Id       string   `json:"id"`
	Speakers Speakers `json:"speakers"`
	Title    string   `json:"title"`
	Type     string   `json:"talkType"`
	Track    string   `json:"track"`
	Summary  string   `json:"summaryAsHtml"`
}

type Talks []Talk

type ScheduleSlot struct {
	Day            string `json:"day"`
	ToTimeMillis   int    `json:"toTimeMillis"`
	FromTimeMillis int    `json:"fromTimeMillis"`
	Talk           Talk   `json:"talk"`
}

type ScheduleSlots []ScheduleSlot

type Schedule struct {
	Slots ScheduleSlots `json:"slots"`
}

type Conference struct {
	BaseUrl      string
	ConferenceId string
}

func DevoxxUK2015() Conference {
	/*return Conference{
		BaseUrl:      "http://cfp.devoxx.co.uk/api/conferences",
		ConferenceId: "UK2015",
	}*/
	return Conference{
		BaseUrl:      "http://cfp.devoxx.be/api/conferences",
		ConferenceId: "DV15",
	}

}

func (c Conference) makeUrl(path ...string) string {
	url, err := url.Parse(c.BaseUrl)
	if err != nil {
		log.Fatalf("Failed to parse base url: %v", err)
	}
	url.Path += "/" + c.ConferenceId
	for _, p := range path {
		url.Path += "/" + p
	}
	return url.String()
}

// use the rest api to get a talk
func (c Conference) GetTalk(talkId string) (Talk, error) {
	var buf Talk
	err := rest.Get(&buf, c.makeUrl("talks", talkId), nil)
	return buf, err
}

func (c Conference) GetScheduleSlots() (ScheduleSlots, error) {
	var slots ScheduleSlots
	for _, day := range []string{"monday", "tuesday", "wednesday", "thursday", "friday"} {
		var buf Schedule
		if err := rest.Get(&buf, c.makeUrl("schedules", day), nil); err != nil {
			return nil, err
		}

		slots = append(slots, buf.Slots...)
	}
	return slots, nil
}

func (c Conference) GetSlotsWithTalks() (ScheduleSlots, error) {
	var slots ScheduleSlots
	allSlots, err := c.GetScheduleSlots()
	if err != nil {
		return nil, err
	}
	for _, slot := range allSlots {
		if slot.Talk.Id != "" {
			slots = append(slots, slot)
		}
	}
	return slots, nil
}

func (s Speaker) GetUuid() string {
	urlParts := strings.Split(s.Link.Href, "/")
	return urlParts[len(urlParts)-1]
}
