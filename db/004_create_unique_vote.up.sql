CREATE INDEX ON vote (talkId, userId);
CREATE INDEX ON vote (talkId);
CREATE INDEX ON vote (userId);
CREATE INDEX ON vote (rating, created);
CREATE INDEX ON vote (created);

-- remember folks postgres uses 1 base array indexing WAT!
CREATE VIEW unique_vote AS
  --SELECT DISTINCT ON (talkId, userId)
  SELECT
    talkId,
    userId,
    (array_agg(rating ORDER BY created))[1] as rating
  FROM vote
  GROUP BY talkId, userId;

CREATE MATERIALIZED VIEW toptalks AS
  SELECT vote.talkId as talkid,
         avg(vote.rating) as avg,
         count(vote.rating) as count,
         sum(vote.rating) as sum,
         talk.title as title,
         talk.type as type,
         talk.track as track,
         talk.summary as summary,
         array_agg(distinct speaker.name) as speakers
  FROM unique_vote AS vote, talk, talk_by, speaker
  WHERE vote.talkId = talk.talkId AND talk_by.talkId = talk.talkId AND speaker.uuid = talk_by.speakerId
  GROUP BY talk.talkId, vote.talkId
  ORDER BY avg DESC;
