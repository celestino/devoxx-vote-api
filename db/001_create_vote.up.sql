-- create the vote table
CREATE TABLE TALK (
  talkId   varchar(10) PRIMARY KEY,
  startTime timestamp NOT NULL
);

CREATE TABLE VOTE (
  id      SERIAL PRIMARY KEY,
  created timestamp DEFAULT current_timestamp,
  rating  integer NOT NULL CHECK (rating > 0 AND rating < 6),
  talkId  varchar(10) NOT NULL REFERENCES TALK (talkId),
  userId  integer NOT NULL CHECK (userId > 0),
  ip      cidr
);

CREATE TABLE SPEAKER (
  uuid char(41) PRIMARY KEY,
  name text NOT NULL
);

CREATE TABLE TALK_BY (
  talkId varchar(10) REFERENCES TALK (talkId),
  speakerId char(41) REFERENCES SPEAKER (uuid)
);
